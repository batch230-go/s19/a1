//console.log("Hello!");

let username = prompt("Enter your username: ").toLowerCase();
let password = prompt("Enter your password: ").toLowerCase();
let role = prompt("Enter your role: ").toLowerCase();


if((username == "" || username == null) || (password == "" || password == null) || (role == "" || role == null)){
    alert("Input must not be empty");
}
else{
    switch(role){
        case "admin":
            alert("Welcome back to the class portal, admin!");
            break;
        case "teacher":
            alert("Thank you for logging in, teacher!");
            break;
        case "student":
            alert("Welcome to the class portal, student!");
            function checkAverage(score1, score2, score3, score4){
                let studentAverage = Math.round((score1 + score2 + score3 + score4)/4);
                if(studentAverage <= 74){
                    console.log("Hello, student, your average is " + studentAverage + ". The letter equivalent is F");
            }
                else if(studentAverage >= 75 && studentAverage <= 79){
                    console.log("Hello, student, your average is " + studentAverage + ". The letter equivalent is D");
            }
                else if(studentAverage >= 80 && studentAverage <= 84){
                    console.log("Hello, student, your average is " + studentAverage + ". The letter equivalent is C");
            }
                else if(studentAverage >= 85 && studentAverage <= 89){
                    console.log("Hello, student, your average is " + studentAverage + ". The letter equivalent is B");
            }
                else if(studentAverage >= 90 && studentAverage <= 95){
                    console.log("Hello, student, your average is " + studentAverage + ". The letter equivalent is A");
            }
                else{
                    console.log("Hello, student, your average is " + studentAverage + ". The letter equivalent is A+");
            }   
        }
            checkAverage(71, 70, 73, 74);
            break;
        default:
            alert("Role out of range.");
            break;
    }
}



